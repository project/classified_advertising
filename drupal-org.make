api = 2
core = 7.x

; Modules
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc5"

projects[back_to_top][subdir] = "contrib"
projects[back_to_top][version] = "1.5"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.14"

projects[dialog][subdir] = "contrib"
projects[dialog][version] = "1.x-dev"

projects[context][subdir] = "contrib"
projects[context][version] = "3.7"

projects[ds][subdir] = "contrib"
projects[ds][version] = "2.15"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.9"

projects[features][subdir] = "contrib"
projects[features][version] = "2.10"

projects[features_extra][subdir] = "contrib"
projects[features_extra][version] = "1.0"

projects[field_formatter_settings][subdir] = "contrib"
projects[field_formatter_settings][version] = "1.1"

projects[email][subdir] = "contrib"
projects[email][version] = "1.3"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.6"

projects[field_multiple_limit][subdir] = "contrib"
projects[field_multiple_limit][version] = "1.0-alpha5"

projects[flag][subdir] = "contrib"
projects[flag][version] = "3.9"

projects[flag_abuse][subdir] = "contrib"
projects[flag_abuse][version] = "2.0"

projects[flagging_form][subdir] = "contrib"
projects[flagging_form][version] = "3.x-dev"

projects[galleryformatter][subdir] = "contrib"
projects[galleryformatter][version] = "1.4"

projects[gmap][subdir] = "contrib"
projects[gmap][version] = "2.11"

projects[hierarchical_select][subdir] = "contrib"
projects[hierarchical_select][version] = "3.0-beta8"

projects[imagecache_actions][subdir] = "contrib"
projects[imagecache_actions][version] = "1.9"

projects[inline_registration][subdir] = "contrib"
projects[inline_registration][version] = "1.0"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "3.0-alpha5"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.3"

projects[location][subdir] = "contrib"
projects[location][version] = "3.7"

projects[location_taxonomize][subdir] = "contrib"
projects[location_taxonomize][patch][] = "http://drupal.org/files/issues/location_taxonomize-add-location-cck-support-1331052-15.patch"
projects[location_taxonomize][version] = "2.4"

projects[node_export][subdir] = "contrib"
projects[node_export][version] = "3.1"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.3"

projects[realname][subdir] = "contrib"
projects[realname][version] = "1.3"

projects[search_api][subdir] = "contrib"
projects[search_api][version] = "1.24"

projects[search_api_db][subdir] = "contrib"
projects[search_api_db][version] = "1.6"

projects[search_api_ranges][subdir] = "contrib"
projects[search_api_ranges][version] = "1.5"

projects[session_api][subdir] = "contrib"
projects[session_api][version] = "1.0-rc1"

projects[facetapi][subdir] = "contrib"
projects[facetapi][version] = "1.5"

projects[facetapi_pretty_paths][subdir] = "contrib"
projects[facetapi_pretty_paths][version] = "1.4"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[taxonomy_breadcrumb][subdir] = "contrib"
projects[taxonomy_breadcrumb][patch][] = "http://drupal.org/files/multiple-taxonomy%20terms-breadcrumb-1850040-3.patch"
projects[taxonomy_breadcrumb][version] = "1.x-dev"

projects[sharethis][subdir] = "contrib"
projects[sharethis][version] = "2.13"

projects[taxonomy_csv][subdir] = "contrib"
projects[taxonomy_csv][version] = "5.10"

projects[token][subdir] = "contrib"
projects[token][version] = "1.7"

projects[views_contact_form][subdir] = "contrib"
projects[views_contact_form][version] = "1.6"

projects[uuid][subdir] = "contrib"
projects[uuid][version] = "1.0"

projects[views][subdir] = "contrib"
projects[views][version] = "3.20"

projects[ckeditor][subdir] = "contrib"
projects[ckeditor][version] = "1.18"

; Themes
projects[bootstrap][version] = "3.20"

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.3.4/ckeditor_4.3.4_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
