<?php
/**
 * @file
 * classified_advertising_ad.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function classified_advertising_ad_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|classified|teaser_gallery';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'classified';
  $ds_fieldsetting->view_mode = 'teaser_gallery';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'span',
        'class' => '',
      ),
    ),
  );
  $export['node|classified|teaser_gallery'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function classified_advertising_ad_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|classified|teaser_gallery';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'classified';
  $ds_layout->view_mode = 'teaser_gallery';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_ad_photos',
        1 => 'title',
        2 => 'field_ad_price',
        3 => 'field_ad_category',
      ),
    ),
    'fields' => array(
      'field_ad_photos' => 'ds_content',
      'title' => 'ds_content',
      'field_ad_price' => 'ds_content',
      'field_ad_category' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|classified|teaser_gallery'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function classified_advertising_ad_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_gallery';
  $ds_view_mode->label = 'Teaser gallery';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['teaser_gallery'] = $ds_view_mode;

  return $export;
}
