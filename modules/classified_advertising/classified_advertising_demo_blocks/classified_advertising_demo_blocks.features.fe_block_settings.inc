<?php
/**
 * @file
 * classified_advertising_demo_blocks.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function classified_advertising_demo_blocks_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-about_classified_advertising'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'about_classified_advertising',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootstrap_classified_advertising' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap_classified_advertising',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'About Classified Advertising',
    'visibility' => 0,
  );

  $export['block-contact_us'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'contact_us',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootstrap_classified_advertising' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap_classified_advertising',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Contact Us',
    'visibility' => 0,
  );

  $export['block-follow_us'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'follow_us',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootstrap_classified_advertising' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap_classified_advertising',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Follow us',
    'visibility' => 0,
  );

  $export['block-useful_links'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'useful_links',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootstrap_classified_advertising' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap_classified_advertising',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Useful Links',
    'visibility' => 0,
  );

  return $export;
}
