<?php
/**
 * @file
 * classified_advertising_demo_blocks.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function classified_advertising_demo_blocks_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'About Classified Advertising';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'about_classified_advertising';
  $fe_block_boxes->body = '<p><strong>Classifieds advertising</strong>&nbsp;is a distribution for building Drupal based Classified Ads Platforms. Classifieds advertising plan to built the OLX clone. Join our official Drupal <a href="https://groups.drupal.org/classified-advertising" target="_blank">Group</a></p>
';

  $export['about_classified_advertising'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Contact Us';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'contact_us';
  $fe_block_boxes->body = '<p>Lorem ipsum dolor sit amet, mel lucilius ocurreret reprimique ex, fastidii convenire no vis. Ea pri nostrud imperdiet, te nam illum efficiantur.</p>
';

  $export['contact_us'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Follow us';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'follow_us';
  $fe_block_boxes->body = '<div class="list-group"><a class="list-group-item" href="#">Facebook Page</a> <a class="list-group-item" href="#">Twitter Account</a> <a class="list-group-item" href="#">Instagram</a></div>
';

  $export['follow_us'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Useful Links';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'useful_links';
  $fe_block_boxes->body = '<div class="list-group"><a class="list-group-item" href="#">Our Blog</a> <a class="list-group-item" href="#">About Us</a> <a class="list-group-item" href="#">Terms &amp; Conditions</a></div>
';

  $export['useful_links'] = $fe_block_boxes;

  return $export;
}
